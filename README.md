# gnucash-cs-common

GnuCash Integration Utilities in C#. A companion package to GnuCash CSharp Rest project.

# Fraction
Allows to use fractions in a simple manner. Implements all basic operations (add, subtract, multiply, divide, compare, simplyfy) related to fractions. Allows conversion to/from string/double.

Implements extensions that allow utilizing fractions in LINQ, for example `fractions.Sum()` or `items.Sum(x => x.FractionProperty)`
