using System;
using System.Collections.Generic;
using System.Linq;

namespace GnuCashCs.Utilities
{
    public static class FractionExtensions
    {
        public static Fraction Sum(this IEnumerable<Fraction> source)
        {
            return source.Aggregate((x, y) => x + y);
        }

        public static Fraction Sum<T>(this IEnumerable<T> source, Func<T, Fraction> selector)
        {
            return source.Select(selector).Aggregate((x, y) => x + y);
        }
    }
}