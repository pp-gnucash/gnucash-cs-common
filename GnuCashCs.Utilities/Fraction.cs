﻿using System;

namespace GnuCashCs.Utilities
{
    public struct Fraction
    {
        public long Numerator { get; set; }
        public long Denominator { get; set; }

        public Fraction(long numerator, long denominator)
            : this()
        {
            if (numerator < 0 && denominator < 0 ||
                denominator < 0)
            {
                //Flip signs if we have two negatives or negative on denominator only
                Numerator = -numerator;
                Denominator = -denominator;
            }
            else if (denominator == 0)
            {
                throw new ArgumentException("Denominator cannot be zero");
            }
            else
            {
                Numerator = numerator;
                Denominator = denominator;
            }
        }

        public Fraction Simplify()
        {
            long gcd = GCD();
            return new Fraction(Numerator / gcd, Denominator / gcd);
        }

        public Fraction InTermsOf(Fraction other)
        {
            return Denominator == other.Denominator ? this :
                new Fraction(Numerator * other.Denominator, Denominator * other.Denominator);
        }

        public long GCD()
        {
            long a = Math.Abs(Numerator);
            long b = Math.Abs(Denominator);
            while (b != 0)
            {
                long t = b;
                b = a % b;
                a = t;
            }
            return a;
        }

        public Fraction Reciprocal()
        {
            return new Fraction(Denominator, Numerator);
        }


        public static Fraction operator +(Fraction left, Fraction right)
        {
            var left2 = left.InTermsOf(right);
            var right2 = right.InTermsOf(left);

            return (new Fraction(left2.Numerator + right2.Numerator, left2.Denominator)).Simplify();
        }

        public static Fraction operator -(Fraction left, Fraction right)
        {
            var left2 = left.InTermsOf(right);
            var right2 = right.InTermsOf(left);

            return (new Fraction(left2.Numerator - right2.Numerator, left2.Denominator)).Simplify();
        }

        public static Fraction operator *(Fraction left, Fraction right)
        {
            return (new Fraction(left.Numerator * right.Numerator, left.Denominator * right.Denominator)).Simplify();
        }

        public static Fraction operator /(Fraction left, Fraction right)
        {
            return (new Fraction(left.Numerator * right.Denominator, left.Denominator * right.Numerator)).Simplify();
        }

        public static bool operator ==(Fraction left, Fraction right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Fraction left, Fraction right)
        {
            return !left.Equals(right);
        }

        public static implicit operator Fraction(string value)
        {
            if (value == null)
                return null;
            else if (string.IsNullOrEmpty(value))
                return new Fraction(0, 1);

            var tokens = value.Split('/');
            long num;
            long den;
            if (tokens.Length == 1 && long.TryParse(tokens[0], out num))
            {
                return new Fraction(num, 1);
            }
            else if (tokens.Length == 2 && long.TryParse(tokens[0], out num) && long.TryParse(tokens[1], out den))
            {
                return new Fraction(num, den);
            }
            throw new Exception("Invalid fraction format");
        }

        public static implicit operator Fraction(double d)
        {
            return ApproximateFraction(d);
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}", Numerator, Denominator);
        }

        public double ToDouble(int decimalPlaces)
        {
            if (this.Denominator == 0)
                return double.NaN;

            return System.Math.Round(
                Numerator / (double)Denominator,
                decimalPlaces
            );
        }

        public decimal ToDecimal(int decimalPlaces)
        {
            if (this.Denominator == 0)
                throw new ArgumentException();

            return System.Math.Round(
                Numerator / (decimal)Denominator,
                decimalPlaces
            );
        }

        /// <summary>
        /// Approximates the provided value to a fraction.
        /// http://stackoverflow.com/questions/95727/how-to-convert-floats-to-human-readable-fractions
        /// </summary>
        private static Fraction ApproximateFraction(double value)
        {
            const double EPSILON = .000001d;

            long n = 1;  // numerator
            long d = 1;  // denominator
            double fraction = n / d;

            while (System.Math.Abs(fraction - value) > EPSILON)
            {
                if (fraction < value)
                {
                    n++;
                }
                else
                {
                    d++;
                    n = (long)System.Math.Round(value * d);
                }

                fraction = n / (double)d;
            }

            return new Fraction(n, d);
        }

        public override bool Equals(object obj)

        {
            return Equals(obj as Fraction?);
        }

        public bool Equals(Fraction? obj)
        {
            if (!obj.HasValue)
                return false;

            var a = obj.Value.Simplify();
            var b = (new Fraction(this.Numerator, this.Denominator)).Simplify();

            return a.Numerator == b.Numerator && a.Denominator == b.Denominator;
        }

        public override int GetHashCode()
        {
            var hashCode = 352033288;
            hashCode = hashCode * -1521134295 + this.Numerator.GetHashCode();
            hashCode = hashCode * -1521134295 + this.Denominator.GetHashCode();
            return hashCode;
        }
    }
}
