using System;
using Xunit;

namespace GnuCashCs.Utilities.Tests
{
    public class FractionTests
    {
        [Theory]
        [InlineData(3, 4, 3, 4)] //All positive
        [InlineData(-3, 4, -3, 4)] //Negative Numerator
        [InlineData(3, -4, -3, 4)] //Negative Denominator
        [InlineData(-3, -4, 3, 4)] //Two negatives
        public void Constructor(long num, long denom, long expectedNum, long expectedDenom)
        {
            //Arrange/Act
            var fraction = new Fraction(num, denom);

            //Assert
            Assert.Equal(expectedNum, fraction.Numerator);
            Assert.Equal(expectedDenom, fraction.Denominator);
        }

        [Theory]
        [InlineData("46410/156", 595, 2)]
        [InlineData("-46410/156", -595, 2)]
        [InlineData("46410/-156", -595, 2)]
        [InlineData("-46410/-156", 595, 2)]
        [InlineData("15496657848/500000", 1937082231, 62500)]
        public void Simplify(string str, long expectedNum, long expectedDenom)
        {
            //Arrange
            Fraction fraction = str;

            //Act
            fraction = fraction.Simplify();

            //Assert
            Assert.Equal(expectedNum, fraction.Numerator);
            Assert.Equal(expectedDenom, fraction.Denominator);
        }

        [Fact]
        public void InTermsOf()
        {
            //Arrange
            Fraction fA = "3/4";
            Fraction fB = "7/8";

            //Act
            fA = fA.InTermsOf(fB);

            //Assert
            Assert.Equal(24, fA.Numerator);
            Assert.Equal(32, fA.Denominator);
        }

        [Fact]
        public void GreatestCommonDivisor()
        {
            //Arrange
            Fraction f = "46410/156";

            //Act
            var gcd = f.GCD();

            //Assert
            Assert.Equal(78, gcd);
        }

        [Fact]
        public void Reciprocal()
        {
            //Arrange
            Fraction f = "46410/156";

            //Act
            var r = f.Reciprocal();

            //Assert
            Assert.Equal(156, r.Numerator);
            Assert.Equal(46410, r.Denominator);
        }

        [Theory]
        [InlineData("6/14", "6/10", 36, 35)] //Two positives
        [InlineData("-6/14", "6/10", 6, 35)] //First negative
        [InlineData("6/14", "-6/10", -6, 35)] //Second negative
        [InlineData("-6/14", "-6/10", -36, 35)] //Two negatives
        public void Addition(string aStr, string bStr, long expectedNum, long expectedDenom)
        {
            //Arrange
            Fraction a = aStr;
            Fraction b = bStr;

            //Act
            Fraction result = a + b;

            //Assert
            Assert.Equal(expectedNum, result.Numerator);
            Assert.Equal(expectedDenom, result.Denominator);
        }

        [Theory]
        [InlineData("6/14", "6/10", -6, 35)] //Two positives
        [InlineData("-6/14", "6/10", -36, 35)] //First Negative
        [InlineData("6/14", "-6/10", 36, 35)] //Second Negative
        [InlineData("-6/14", "-6/10", 6, 35)] //Two negatives
        public void Subtraction(string aStr, string bStr, long expectedNum, long expectedDenom)
        {
            //Arrange
            Fraction a = aStr;
            Fraction b = bStr;

            //Act
            Fraction result = a - b;

            //Assert
            Assert.Equal(expectedNum, result.Numerator);
            Assert.Equal(expectedDenom, result.Denominator);
        }

        [Theory]
        [InlineData("6/14", "6/10", 9, 35)] //Two positives
        [InlineData("-6/14", "6/10", -9, 35)] //First Negative
        [InlineData("6/14", "-6/10", -9, 35)] //Second Negative
        [InlineData("-6/14", "-6/10", 9, 35)] //Two negatives
        [InlineData("59374168/100", "261/5000", 1937082231, 62500)] //Large numbers
        public void Multiplication(string aStr, string bStr, long expectedNum, long expectedDenom)
        {
            //Arrange
            Fraction a = aStr;
            Fraction b = bStr;

            //Act
            Fraction result = a * b;

            //Assert
            Assert.Equal(expectedNum, result.Numerator);
            Assert.Equal(expectedDenom, result.Denominator);
        }

        [Theory]
        [InlineData("6/14", "6/10", 5, 7)] //Two positives
        [InlineData("-6/14", "6/10", -5, 7)] //First Negative
        [InlineData("6/14", "-6/10", -5, 7)] //Second Negative
        [InlineData("-6/14", "-6/10", 5, 7)] //Two negatives
        public void Division(string aStr, string bStr, long expectedNum, long expectedDenom)
        {
            //Arrange
            Fraction a = aStr;
            Fraction b = bStr;

            //Act
            Fraction result = a / b;

            //Assert
            Assert.Equal(expectedNum, result.Numerator);
            Assert.Equal(expectedDenom, result.Denominator);
        }

        [Theory]
        [InlineData("6/14", "3/7", true)]
        [InlineData("6/14", "2/7", false)]
        public void EqualsOperator(string aStr, string bStr, bool expectedEquals)
        {
            //Arrange
            Fraction a = aStr;
            Fraction b = bStr;

            //Act
            bool result = a == b;

            //Assert
            Assert.Equal(expectedEquals, result);
        }

        [Theory]
        [InlineData("6/14", "3/7", true)]
        [InlineData("6/14", "2/7", false)]
        public void NotEqualsOperator(string aStr, string bStr, bool expectedEquals)
        {
            //Arrange
            Fraction a = aStr;
            Fraction b = bStr;

            //Act
            var result = a != b;

            //Assert
            Assert.Equal(expectedEquals, !result);
        }

        [Theory]
        [InlineData("3/4", 3, 4)] //Two positives
        [InlineData("-3/4", -3, 4)] //Negative Num
        [InlineData("3/-4", -3, 4)] //Negative Denom
        [InlineData("-3/-4", 3, 4)] //Two negatives
        [InlineData("3", 3, 1)] //Integer
        [InlineData("-3", -3, 1)] //Negative Integer
        public void ImplicitOperatorFromString(string str, long expectedNum, long expectedDenom)
        {
            //Arrange/Act
            Fraction fraction = str;

            //Assert
            Assert.Equal(expectedNum, fraction.Numerator);
            Assert.Equal(expectedDenom, fraction.Denominator);
        }

        [Theory]
        [InlineData(3.25d, 13, 4)]
        [InlineData(3d, 3, 1)]
        public void ImplicitOperatorFromDouble(double num, long expectedNum, long expectedDenom)
        {
            //Arrange/Act
            Fraction fraction = num;

            //Assert
            Assert.Equal(expectedNum, fraction.Numerator);
            Assert.Equal(expectedDenom, fraction.Denominator);
        }

        [Theory]
        [InlineData(3, 4, "3/4")]
        [InlineData(3, -4, "-3/4")]
        [InlineData(12345678912345, 12345678912345, "12345678912345/12345678912345")]
        public void ToStringTest(long num, long denom, string expected)
        {
            //Arrange
            var fraction = new Fraction(num, denom);

            //Act
            var str = fraction.ToString();

            //Assert
            Assert.Equal(expected, str);
        }

        [Theory]
        [InlineData("7302/64", 3, 114.094d)]
        [InlineData("-7302/64", 4, -114.0938d)]
        public void ToDoubleTest(string f, int decimals, double expected)
        {
            //Arrange
            Fraction fraction = f;

            //Act
            var result = fraction.ToDouble(decimals);

            //Assert
            Assert.Equal(expected, result); 
        }

        [Theory]
        [InlineData("1937082231/62500", 4, 30993.3157)]
        [InlineData("-1937082231/62500", 3, -30993.316)]
        public void ToDecimalTest(string f, int decimals, decimal expected)
        {
            //Arrange
            Fraction fraction = f;

            //Act
            var result = fraction.ToDecimal(decimals);

            //Assert
            Assert.Equal(expected, result); 
        }


        [Fact]
        public void GetHashCodeTest() {
            //Arrange
            Fraction f = "3/4";

            var expected = 352033288;
            expected = expected * -1521134295 + 3.GetHashCode();
            expected = expected * -1521134295 + 4.GetHashCode();

            //Act
            var hash = f.GetHashCode();

            //Arrange
            Assert.Equal(expected, hash);
        }
    }
}
