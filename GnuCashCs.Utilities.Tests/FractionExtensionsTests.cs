using System;
using System.Collections.Generic;
using Xunit;

namespace GnuCashCs.Utilities.Tests
{
    public class FractionExtensionsTests
    {
        [Fact]
        public void SumOfEnumerableFractions()
        {
            //Arrange
            var list = new List<Fraction>()
            {
                new Fraction(3, 4),
                new Fraction(1, 8),
                new Fraction(1, 9)
            };

            //Act
            var sum = list.Sum();

            //Assert
            Assert.Equal(71, sum.Numerator);
            Assert.Equal(72, sum.Denominator);
        }

        [Fact]
        public void SumOfFractionProperty()
        {
            //Arrange
            var items = new List<FractionContainer>() {
                new FractionContainer("Item1", new Fraction(3, 4)),
                new FractionContainer("Item2", new Fraction(1, 8)),
                new FractionContainer("Item3", new Fraction(1, 9))
            };

            //Act
            var sum = items.Sum(i => i.Value);

            //Assert
            Assert.Equal(71, sum.Numerator);
            Assert.Equal(72, sum.Denominator);
        }
    }

    internal class FractionContainer
    {
        public string Name { get; set; }
        public Fraction Value { get; set; }

        public FractionContainer(string name, Fraction value)
        {
            Name = name;
            Value = value;
        }
    }

}
